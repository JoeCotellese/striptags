# Striptags

Striptags is a simple Python script that takes HTML content from the clipboard, strips the HTML tags, and then pastes the cleaned content back to the clipboard.

## Installation

This package uses [Poetry](https://python-poetry.org/) for dependency management. If you haven't installed Poetry yet, you can do so by following the instructions on their [official website](https://python-poetry.org/docs/#installation).

If you want to install this so you can run it system wide you also need pipx.

To install Striptags, you can clone the repository and install it locally:

```sh
git clone https://github.com/JoeCotellese/striptags.git
cd striptags
poetry install
poetry build
pipx install dist/striptags-0.1.0-py3-none-any.whl
```

## Usage

After installing the package, you can run the striptags command to strip HTML tags from the content in your clipboard:

`striptags`

This command will get the content from the clipboard, strip any HTML tags from it, and then copy the cleaned content back to the clipboard.

## Development

To contribute to Striptags, you can clone the repository and install the dependencies:

```sh
git clone https://github.com/yourusername/striptags.git
cd striptags
poetry install
```
