import re
import pyperclip

def striptags(buffer: str) -> str:

    # Use regex to strip HTML tags
    cleaned_content = re.sub('<[^<]+?>', '', buffer)

    return cleaned_content

def main():
    # get the content from the clipboard
    content = pyperclip.paste()
    # test to see if the content is HTML
    content = striptags(content)
    # copy the content back to the clipboard
    pyperclip.copy(content)

if __name__ == '__main__':
    main()
